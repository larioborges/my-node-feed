var fs = require('fs'),
	util = require("./Util.js"),
	FeedData = require("./MyFeedData.js");



// ---------- FILE READING -----------
function getFileReadingPromise(filename, handler) {

	return new Promise(function(resolve, reject) {
		fs.readFile(filename, 'utf8', function(err, data) {
			handler(err, data, resolve, reject);
		});
	});
}

function printUserFileErrorLine(line) {
	console.log("User File Error: line " + line + " not valid");
}

function readUserData(filename) {
	return getFileReadingPromise(filename, function(err, data, resolve, reject) {

		if (err != undefined || err != null) {
			reject("Error reading user data file \'" + filename + "\'");
			return;
		} 

		let users = {}, lines = data.split("\n"), userName, userNames, tempUser, follower;

		for (let i = 0; i < lines.length; i ++) {
			if (util.checkStringNull(lines[i])) {
				continue;
			}

			userNames = lines[i].split(" follows ");

			if (userNames.length != 2)  {
				printUserFileErrorLine(i +1)
				continue;
			}

			userName = userNames[0].trim();
			followers = userNames[1].trim();

			tempUser = users[userName.toLowerCase()];
			if (tempUser == null || tempUser == undefined) {
				users[userName.toLowerCase()] = new FeedData.User(userName);
			}

			if (followers.indexOf(",") == -1) {
				// If the follower is a single user create a follower
				tempUser = users[followers.toLowerCase()];
				if (tempUser == null || tempUser == undefined) {
					users[followers.toLowerCase()] = new FeedData.User(followers);
				}
			}

			// Usernames are assumed case insensitive
			users[userName.toLowerCase()].addFollowers(followers);
		}


		// Return an alphabetical list of User objects
		const usernameArr = Object.keys(users);
		usernameArr.sort();
		let returnUserArr = [];

		for (let i = 0; i < usernameArr.length; i ++) {
			returnUserArr.push(users[usernameArr[i]]);
		}

		resolve(returnUserArr);
	});
}

function printPostFileErrorLine(line) {
	console.log("User File Error: line " + line + " not valid");
}

function readPostData(filename) { 
	return getFileReadingPromise(filename, function(err, data, resolve, reject) {
		if (err != undefined || err != null) {
			reject("Error reading post data file \'" + filename + "\'");
			return;
		} 

		let lines = data.split("\n"), posts = [], userName, postText, postArr, line;

		for (let i = 0; i < lines.length; i ++) {
			line = lines[i];
			if (util.checkStringNull(line)) continue;

			postArr = line.split('>');

			if (postArr.length >= 2) {
				userName = postArr[0];
				// SPECIAL CASE: if ">" is in the text we make sure it is in the text
				postText = line.slice(line.indexOf(">") + 1);

				// POST TOO LONG
				if (postText.length > 140) {
					console.log("Post File Error: line " + (i + 1) + " too long");
					continue;
				}

				posts.push(new FeedData.Post(userName.trim(), postText.trim()));
			} else {
				console.log("Post File Error: line " + (i + 1) + " not valid");
			}
		}
		resolve(posts);
	});
}


// ---------- COMBINE DATA -----------
function outputFeedData(users, posts) {
	for (let i = 0; i < users.length; i ++ ) {
		console.log(users[i].name);
		for (let j = 0; j < posts.length; j ++ ) {
			if (users[i].doesFollow(posts[j].user)) {
				console.log("\t@" + posts[j].user + ": " + posts[j].text);
			}
		}
	}
}


// ---------- RUN APP -----------
let myArgs = process.argv.slice(2), 
	userfile = "user.txt", 
	postfile = "tweet.txt";

if (myArgs.length > 0) {
	userfile = myArgs[0];
	if (myArgs.length > 1) {
		postfile = myArgs[1];
	}
}



var getAllData = Promise.all([readUserData(userfile), readPostData(postfile)]);

getAllData.then(function(data) {
	outputFeedData(data[0], data[1]);
}).catch(function(err) {
	console.log(err);
});