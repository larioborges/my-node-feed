
var util = require("./Util.js");

// ---------- CLASSES -----------
class User {
	constructor(name) {
		this.name = name;
		this.following = [];
	}

	addSingleFollower(follower) {
		if (util.checkStringNull(follower)) return;

		let followingBool = false;

		for (let i = 0; i < this.following.length; i ++) {
			// Usernames are assumed case insensitive
			if (this.following[i].toUpperCase() === follower.toUpperCase()) {
				followingBool = true;
			}
		}

		if (!followingBool) {
			this.following.push(follower);
		}
	}

	addFollowers(followers) {

		if (util.checkStringNull(followers)) return;

		let followerArr = followers.split(",");

		if (followerArr.length > 0) {
			for (let i = 0; i < followerArr.length; i ++) {
				this.addSingleFollower(followerArr[i].trim());				
			}
			return;
		}

		this.addSingleFollower(followers.trim());
	}

	doesFollow(username) {
		if (util.checkStringNull(username)) return false;

		// We follow ourselves
		if (username.toUpperCase() === this.name.toUpperCase()) return true;

		let followingUppercase = this.following.map(function(value) {
	      return value.toUpperCase();
	    });

	    if (followingUppercase.indexOf(username.toUpperCase()) != -1) return true;

	    return false;
	}
}

class Post {
	constructor(user, text) {
		this.user = user;
		this.text = text;
	}
}

module.exports.User = User;
module.exports.Post = Post;