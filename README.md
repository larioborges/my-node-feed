
This application is a NojeJS applications reads user and post datafiles. The filenames are taken as command line arguements. To run the application you will need to have NodeJS installed.

1. Clone the repo
2. Type "node main.js" to run the app with default files user.txt and tweet.txt
3. For command line arguements run "node main.js userfilepath postfilepath"

If there are erroneous lines in the files the feedback will just be printed to the command line.